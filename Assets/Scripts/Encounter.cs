﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Encounter
{
    public int arenaLevel = 0;
    // list of rounds of monsters
    public List<List<Monster>> monsters;
}
