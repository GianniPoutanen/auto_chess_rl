﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Monster : Piece
{
    // check if players piece or not
    public bool isEvil = false;

    public enum classes
    {
        warrior,
        ranger,
        mage
    }

    public enum sign
    {
        basic,
        forrest,
        fire,
        ice
    }

    // Piece stats that can be represented as an float value
    Dictionary<string, float> Stats = new Dictionary<string, float>();

    // [0] = y, [1] = x
    public int[] fieldLocation = new int[2];
    private int[] targetLocation = new int[2];
    private GameObject target;
    LinkedList<int[]> path;
    public Field field;

    // Combat variables
    // unused private bool canAttack = true;
    private bool inRange = false;
    private float attackTimer = -1;
    private List<Item> equipedItems = new List<Item>();

    // Gamemanager and Animation variables
    public Animator animator;
    public GameManager gameManager;


    private void Start()
    {
        // Base stats
        AddStat("MaxHealth", 20);
        AddStat("Armor", 1);
        AddStat("Damage", Random.Range(2f, 5f));
        AddStat("Range", 1);
        AddStat("Speed", Random.Range(0.3f, 1.4f));
        AddStat("AttackSpeed", Random.Range(0.1f, 1f));
        AddStat("CriticalChance", 0.2f);
        AddStat("CriticalDamage", 6);
        AddStat("LifeSteal", 0.2f);
        AddStat("ManaPool", 24);
        AddStat("ManaRegen", 6);
        AddStat("SpellPower", 10);
        AddStat("Size", 2);

        // Collect variables
        this.animator = this.GetComponent<Animator>();
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        attackTimer = Time.time;
        this.field = GameObject.Find("Field").GetComponent<Field>();
        this.field.currentField[this.fieldLocation[0], this.fieldLocation[1]] = this.gameObject;

        // set scale
        this.transform.localScale = new Vector3(isEvil ? -Stats["Size"] : Stats["Size"], Stats["Size"], Stats["Size"]);

        // applies items effects onto stats
        ApplyItemEffects();

        // Set current health to Max health last just in case of items
        AddStat("CurrentHealth", this.Stats["MaxHealth"]);

    }
    // on in game tick update
    void Update()
    {
        if (gameManager.CurrentState == GameManager.GameState.StartBattle)
        {
            if (this.inRange)
            {
                AttackTarget();
            }
            else
            {
                StepTowardClosestEnemy();
            }

        }
    }

    public void AttackTarget()
    {
        if (attackTimer < Time.time)
        {
            attackTimer = Time.time + this.Stats["AttackSpeed"];
            if (this.target != null)
            {
                this.animator.SetTrigger("attack");
            }
        }

        // Check if target still exists
        if (this.target == null)
        {
            this.inRange = false;
        }

        // Check still in range
        if (CheckTargetInRange())
        {
            this.inRange = false;
            return; // break function
        }
    }

    public void TakeDamage(float value)
    {
        if (this.Stats["CurrentHealth"] - value > 0)
        {
            this.Stats["CurrentHealth"] -= value;
        } 
        else
        {
            // cause death event
            this.Stats["CurrentHealth"] = 0;
            this.field.RemovePiece(this, isEvil);
            Destroy(this.gameObject); // ominous
            Destroy(this); // ominous
        }
    }

    public void DamageTarget()
    {
        if (this.target != null)
        {
            this.target.GetComponent<Monster>().TakeDamage(this.Stats["Damage"]);
        }
    }

    public void ApplyItemEffects()
    {
        foreach (Item item in this.equipedItems)
        {
            foreach (string key in item.effectedStats.Keys)
            {
                this.AddStat(key, item.effectedStats[key]);
            }
        }
    }

    public void StepTowardClosestEnemy()
    {
        if (this.gameObject.name == "Old_Man(Clone) ally 3")
        {

        }


        if (this.target == null || path == null || path.Count == 0)
        {
            // Get closest target
            this.target = this.field.GetClosestTarget(this.isEvil, this);

            // Check if target is in range (+ 2 as start pos and end pos are taken up by the two monsters)
            if (CheckTargetInRange())
            {
                this.inRange = true;
                animator.SetBool("isWalking", false);
                return; // break function
            }

            if (this.target != null)
            {
                Debug.Log(this.name + " - is targeting - " + this.target.name);
                // Get targets location
                this.targetLocation = this.target.GetComponent<Monster>().fieldLocation;

                if (this.fieldLocation[0] - targetLocation[0] >= 0)
                {
                    this.transform.localScale = new Vector3(-Stats["Size"], Stats["Size"], Stats["Size"]);
                }
                else
                {
                    this.transform.localScale = new Vector3(Stats["Size"], Stats["Size"], Stats["Size"]);
                }
                // Get path to target
                List<int[]> tempList = field.GetPath(this.fieldLocation, this.target.GetComponent<Monster>().fieldLocation);
                // Turn List into LinkedList for path and start walking animation
                if (tempList != null && tempList.Count > 0)
                {
                    if (this.name == "Old_Man(Clone) evil 1")
                    {
                    }
                    path = tempList != null ? new LinkedList<int[]>(tempList) : null;
                    for (int rangeCounter = 0; rangeCounter < this.Stats["Range"]; rangeCounter++) path.RemoveLast(); // target location
                    tempList.RemoveAt(0);
                    tempList.RemoveAt(tempList.Count - 1);
                    if (path != null && this.field.CheckObstruction(tempList))
                        animator.SetBool("isWalking", true);
                }
            }
        }
        else if (!new Vector2(this.transform.position.x, this.transform.position.y).Equals(field.GetTileMidPoint(path.First.Value)))
        {
            if (!this.animator.GetBool("isWalking")) this.animator.SetBool("isWalking", true);
            this.transform.position = Vector2.MoveTowards(this.transform.position, field.GetTileMidPoint(path.First.Value), Stats["Speed"] * Time.deltaTime);
        }
        else
        {
            // Check change to target position or if target is null
            if (path != null && !(this.isEvil ? this.field.allyArmy.Contains(this.target) : this.field.evilArmy.Contains(this.target)))
            {
                this.target = null;
                path = null;
            }
            if (this.target != null && this.targetLocation[0] != this.target.GetComponent<Monster>().fieldLocation[0] &&
                this.targetLocation[1] != this.target.GetComponent<Monster>().fieldLocation[1])
            {
                path = null;
            }

            if (path != null)
            {
                // TODO remove path if obstructed. Check current field for ubstruction
                bool skipFirst = false;
                // Check if all spaces on path is free
                foreach (int[] pos in path)
                {
                    if (skipFirst)
                    {
                        if (field.currentField[pos[0], pos[1]] != null)
                        {
                            path.Clear();
                            break;
                        }
                    }
                    else
                    {
                        skipFirst = true;
                    }
                }

                // changes position values
                if (path.Count > 0)
                {
                    int[] tempLocation = new int[] { this.fieldLocation[0], this.fieldLocation[1] };
                    path.RemoveFirst();
                    if (path.Count > 0)
                    {
                        this.fieldLocation = path.First.Value;
                        field.UpdateFieldPosition(tempLocation, this.fieldLocation, this.gameObject);
                    }
                }

                // wait if path is empty
                if (path.Count == 0)
                {
                    animator.SetBool("isWalking", false);
                }
            }

        }
        // set y value for illusion of depth
        this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, 20 + this.transform.position.y);

    }

    // temp
    int[] rand;
    public void StepAlongRandomPath()
    {
        if (attackTimer < Time.time)
        {
            if (path == null || path.Count == 0)
            {
                rand = field.GetRandomFieldPosition();
                if (this.fieldLocation[0] - rand[0] > 0)
                {
                    this.transform.localScale = new Vector3(-Stats["Size"], Stats["Size"], Stats["Size"]);
                }
                else
                {
                    this.transform.localScale = new Vector3(Stats["Size"], Stats["Size"], Stats["Size"]);
                }
                List<int[]> tempList = field.GetPath(this.fieldLocation, rand);
                path = tempList != null ? new LinkedList<int[]>(tempList) : null;
                animator.SetBool("isWalking", true);
            }
            else if (!new Vector2(this.transform.position.x, this.transform.position.y).Equals(field.GetTileMidPoint(path.First.Value)))
            {
                this.transform.position = Vector2.MoveTowards(this.transform.position, field.GetTileMidPoint(path.First.Value), Stats["Speed"] * Time.deltaTime);
            }
            else
            {

                // TODO remove path if obstructed. Check current field for ubstruction
                bool skipFirst = false;
                // Check if all spaces on path is free
                foreach (int[] pos in path)
                {
                    if (skipFirst)
                    {
                        if (field.currentField[pos[0], pos[1]] != null)
                        {
                            path.Clear();
                            Debug.Log(this.gameObject.name);
                            attackTimer = Time.time + 2;
                            break;
                        }
                    }
                    else
                    {
                        skipFirst = true;
                    }
                }

                // changes position values
                if (path.Count > 0)
                {
                    int[] tempLocation = new int[] { this.fieldLocation[0], this.fieldLocation[1] };
                    path.RemoveFirst();
                    if (path.Count > 0)
                    {
                        this.fieldLocation = path.First.Value;
                        field.UpdateFieldPosition(tempLocation, this.fieldLocation, this.gameObject);
                    }
                }

                // wait if path is empty
                if (path.Count == 0)
                {
                    animator.SetBool("isWalking", false);
                    attackTimer = Time.time + 2;
                }
            }
            // set y value for illusion of depth
            this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, 20 + this.transform.position.y);
        }
    }

    private bool CheckTargetInRange()
    {
        if (target != null)
        {
            List<int[]> tempPath = this.field.GetPath(this.fieldLocation, target.GetComponent<Monster>().fieldLocation);
            if (tempPath != null && tempPath.Count < (this.Stats["Range"] + 2)) return true;
        }
        return false;
    }

    public void AddStat(string stat, float value)
    {
        Stats.Add(stat, value);
    }

    public bool EditStat(string stat, float value)
    {
        if (!Stats.ContainsKey(stat)) return false;
        Stats[stat] = value;
        return true;
    }

    public float GetStat(string stat)
    {
        return Stats[stat];
    }
    public float ReduceStat(string stat, float value)
    {
        if (!Stats.ContainsKey(stat)) return -999;
        Stats[stat] -= value;
        return Stats[stat];
    }
}
