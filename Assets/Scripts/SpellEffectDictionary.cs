﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpellEffectDictionary : MonoBehaviour
{
    // Spell Effect Objects
    public GameObject lightningEffectObject;


    // Spell effect enum for each spell effect
    public enum Spell
    {
        LightningBolt
    }

    public void CastSpellOnTarget(Monster target, Spell spell, float value)
    {
        switch (spell)
        {
            case Spell.LightningBolt:
                StartCoroutine(LightningBolt(target, value, 5)) ;
                break;
        }
    }

    private IEnumerator LightningBolt(Monster target, float damage, int strikes)
    {
        WaitForSeconds waitTimer = new WaitForSeconds(0.1f);
        float boltDeathTimer = 0.7f;
        for (int i = 0; i < strikes; i++)
        {
            GameObject bolt = Instantiate(lightningEffectObject);
            bolt.transform.GetChild(0).transform.position = target.transform.position + new Vector3(0, 3, 0);
            bolt.transform.GetChild(1).transform.position = target.transform.position;
            bolt.GetComponent<LightningBoltScript>().trigger = true;
            bolt.GetComponent<LightningBoltScript>().deathTimer = true;
            bolt.GetComponent<LightningBoltScript>().boltLifeTime = Time.time + boltDeathTimer;
            target.TakeDamage(damage);
            yield return waitTimer ;
        }
    }
}
