﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

// Item base class   
public class Item
{
    public string itemName = "";
    public string itemDescription = "";
    public int cost = 0;

    // Stats effect
    public Dictionary<string, float> effectedStats;
}

