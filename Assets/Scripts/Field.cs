﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Field : MonoBehaviour
{
    //field layout type
    public enum FieldType
    {
        Standard,
        Diamond
    }

    public FieldType type;

    public Sprite baseTileSprite;
    public GameObject baseTile;
    public GameObject gameCam; // attatched camera in scene
    public GameManager gameManager; // attatched camera in scene
    public GameObject[,] currentField; // game objects added to the field
    // variables for keeping track of different pieces
    public List<GameObject> allyArmy;
    public List<GameObject> evilArmy;
    // just in case we want to incorperate specific tile layouts
    public GameObject[,] fieldTiles;
    public int length;
    public int height;
    // offset of tile positions
    public float xOffset;
    public float yOffset;

    // time before round starts
    public float roundWoundup;

    private void Start()
    {
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();

        roundWoundup = Time.time + 2;
    }

    private void Update()
    {
        if (roundWoundup < Time.time && gameManager.CurrentState == GameManager.GameState.NoBattle)
        {
            gameManager.CurrentState = GameManager.GameState.StartBattle;
        }

        if (evilArmy != null)
        {
            if (evilArmy.Count == 0)
            {
                gameManager.CurrentState = GameManager.GameState.GoodWins;
            }
        }
        if (allyArmy != null)
        {
            if (allyArmy.Count == 0)
            {
                gameManager.CurrentState = GameManager.GameState.EvilWins;
            }
        }
    }

    // creates the field given the two armies
    public void InitialiseField(List<GameObject> playerArmy, List<GameObject> enemyArmy, int level)
    {
        ResizeField(level);
        allyArmy = playerArmy;
        evilArmy = enemyArmy;
        currentField = new GameObject[length, height];
        if (allyArmy != null)
        {
            foreach (GameObject obj in allyArmy)
            {
                Monster m = obj.GetComponent<Monster>();
                currentField[m.fieldLocation[0], m.fieldLocation[1]] = obj;
                m.transform.position = GetTileMidPoint(m.fieldLocation) + new Vector3(0, 0, -10);
            }
        }
        if (evilArmy != null)
        {
            foreach (GameObject obj in enemyArmy)
            {
                Monster m = obj.GetComponent<Monster>();
                currentField[m.fieldLocation[0], m.fieldLocation[1]] = obj;
                m.transform.position = GetTileMidPoint(m.fieldLocation) + new Vector3(0, 0, -10);
                //m.transform.localScale = new Vector3(-m.transform.localScale.x, m.transform.localScale.y, m.transform.localScale.z);
            }
        }
        //TEMP
        ColourTiles();
    }

    // creates the field given the two armies
    public void InitialiseField(List<GameObject> playerArmy, List<GameObject> enemyArmy, int l, int h)
    {
        ResizeField(4);
        allyArmy = playerArmy;
        evilArmy = enemyArmy;
        currentField = new GameObject[length, height];
        if (allyArmy != null)
        {
            foreach (GameObject obj in allyArmy)
            {
                Monster m = obj.GetComponent<Monster>();
                currentField[m.fieldLocation[0], m.fieldLocation[1]] = obj;
                m.transform.position = GetTileMidPoint(m.fieldLocation) + new Vector3(0, 0, -10);
            }
        }
        if (evilArmy != null)
        {
            foreach (GameObject obj in enemyArmy)
            {
                Monster m = obj.GetComponent<Monster>();
                currentField[m.fieldLocation[0], m.fieldLocation[1]] = obj;
                m.transform.position = GetTileMidPoint(m.fieldLocation) + new Vector3(0, 0, -10);
                //m.transform.localScale = new Vector3(-m.transform.localScale.x, m.transform.localScale.y, m.transform.localScale.z);
            }
        }
        //TEMP
        ColourTiles();
    }

    // Gets tile under mouse position 
    public GameObject GetTileUnderMouse()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit2D hit = Physics2D.GetRayIntersection(ray, Mathf.Infinity);

        if (hit.collider)
        {
            foreach (GameObject obj in fieldTiles)
            {
                if (obj != null && hit.collider.transform == obj.transform)
                {
                    return obj;
                }
            }
        }
        return null;
    }

    // Gets Monster if any under mouse else returns null
    public GameObject GetObjectUnderMouse()
    {
        GameObject tile = GetTileUnderMouse();
        if (tile != null)
        {
            return this.currentField[tile.GetComponent<TileDescriptor>().coordinates[0], tile.GetComponent<TileDescriptor>().coordinates[1]];
        }
        return null;
    }
        

    // Returns a random field position
    public int[] GetRandomFieldPosition()
    {
        for (int i = 0; i < 50; i++)
        {
            int l = (int)Mathf.Floor((Random.value * length));
            int h = (int)Mathf.Floor((Random.value * height));
            if (fieldTiles[l, h] != null) return new int[] { l, h }; ;
        }
        return null;
    }

    // clears children and lists 
    public void SetMonsterPosition(Monster monster, int[] position)
    {
        currentField[position[0], position[1]] = monster.gameObject;
        ColourTiles();
    }

    // clears children and lists 
    public void ClearPosition(int[] position)
    {
        currentField[position[0], position[1]] = null;
        ColourTiles();
    }

    // clears children and lists 
    public void ClearAll()
    {
        allyArmy.Clear();
        evilArmy.Clear();
        fieldTiles = null;
        currentField = null;
        foreach (Transform child in transform)
        {
            Destroy(child);
        }
    }

    //Creates field and positions camera
    public void ResizeField(int l, int h)
    {
        length = l;
        height = h;
        CreateFieldTiles();
        gameCam.transform.position = new Vector3((l / 2f) - 0.5f + (xOffset * l), (h / 2f) + (yOffset * h), -10f);
        gameCam.transform.localScale = new Vector3((1 + (height * 0.2f)), (1 + (height * 0.2f)), 1);
    }

    //Creates field and positions camera
    public void ResizeField(int level)
    {
        float camSize = 1.5f;
        switch (level)
        {
            case 0:
                length = 3;
                height = 2;
                CreateFieldTiles();
                break;
            case 1:
                length = 3;
                height = 3;
                CreateFieldTiles();
                camSize = 1.8f;
                break;
            case 2:
                // TODO: the diamond one between level 1 and 3
                length = 5;
                height = 3;
                camSize = 2;
                CreateFieldTiles();
                break;
            case 3:
                length = 5;
                height = 3;
                camSize = 2;
                CreateFieldTiles();
                break;
            case 4:
                length = 5;
                height = 4;
                camSize = 2.5f;
                CreateFieldTiles();
                break;
            default:
                break;
        }

        gameCam.transform.position = new Vector3((length / 2f) - 0.5f, height / 2f, -10f);
        gameCam.transform.position = new Vector3((length / 2f) - 0.5f + (xOffset * length / 2), (height / 2f) + (yOffset * height / 2), -10f);
        gameCam.transform.localScale = new Vector3(camSize, camSize, 1);
    }

    // Creates the tiles on the field that serve as the background / board
    void CreateFieldTiles()
    {
        fieldTiles = new GameObject[length, height];
        for (int x = 0; x < length; x++)
        {
            for (int y = 0; y < height; y++)
            {
                if (!(0 == x % 2 && y == 0))
                {
                    GameObject newTile = Instantiate(baseTile);
                    newTile.name = "Tile - x: " + x + ", y: " + y;
                    newTile.GetComponent<TileDescriptor>().coordinates = new int[] { x, y };
                    newTile.transform.parent = this.gameObject.transform;
                    newTile.transform.position = new Vector3(x + (xOffset * x), y + (0.5f * (x % 2)) + (yOffset * y) + ((yOffset * 0.4f) * (x % 2)), 100);
                    fieldTiles[x, y] = newTile;
                }
            }
        }
    }

    // Gets closest target
    public GameObject GetClosestTarget(bool evil, Monster targeter)
    {
        GameObject targetMonster = null;
        int distance = 999;
        int[] initPos = targeter.fieldLocation;
        if (evil)
        {
            foreach (GameObject monster in allyArmy)
            {
                List<int[]> tempPath = GetPath(monster.GetComponent<Monster>().fieldLocation, initPos);
                if (tempPath != null && (targetMonster == null || tempPath.Count <= distance))
                {
                    // check if which game object is closer
                    if (targetMonster == null || Vector3.Distance(targetMonster.transform.position, targeter.transform.position) >
                        Vector3.Distance(monster.transform.position, targeter.transform.position))
                    {
                        targetMonster = monster;
                    }
                    distance = tempPath.Count;
                }

            }
        }
        else
        {
            foreach (GameObject monster in evilArmy)
            {
                List<int[]> tempPath = GetPath(monster.GetComponent<Monster>().fieldLocation, initPos);
                if (tempPath != null && (targetMonster == null || tempPath.Count < distance))
                {
                    // check if which game object is closer
                    if (targetMonster == null || Vector3.Distance(targetMonster.transform.position, targeter.transform.position) >
                        Vector3.Distance(monster.transform.position, targeter.transform.position))
                    {
                        targetMonster = monster;
                    }
                    distance = tempPath.Count;
                }
            }
        }
        return targetMonster;
    }




    // TODO: implement different layours of the standard field.
    /*
    void CreateDiamond()
    {
        fieldTiles = new GameObject[length, height];
        for (int x = 0; x < length; x++)
        {
            for (int y = 0; y < height; y++)
            {
                if (((x <= (length / 2)) && (x >= ((height / 2) - y))) && (x <= ((height / 2) + y)))//  ||
                    //((x >= (length / 2)) && (x >= ((height / 2) - y))) && (x <= ((height / 2) + y)))
                {
                    GameObject newTile = new GameObject("Tile x: " + x + ", y : " + y);
                    newTile.transform.parent = this.gameObject.transform;
                    SpriteRenderer sprite = newTile.AddComponent<SpriteRenderer>();
                    sprite.sprite = baseTileSprite;
                    newTile.transform.position = new Vector3(x, y + (0.5f * (x % 2)), 100);
                    fieldTiles[x, y] = newTile;
                }
            }
        }
    }
    */

    // Gets tiles within the range
    List<int[]> GetTilesInRangeOfPosition(int x, int y, int range)
    {
        List<int[]> tempList = new List<int[]>();
        for (int i = -range; i < x + range; i++)
        {
            for (int j = -range; j < y + range; j++)
            {
                if (OnBoard(x + i, y + j) && i != 0 && j != 0)
                {
                    if ((range + j) < y)
                    {
                        tempList.Add(new int[] { x + j, y + i });
                    }
                    else
                    {
                        if (j < 0)
                        {
                            tempList.Add(new int[] { x + j + i, y + i });
                        }
                        else
                        {
                            tempList.Add(new int[] { x + j - i, y + i });
                        }
                    }
                }
            }
        }
        return tempList;
    }

    // returns a list of field positions from start to end
    public List<int[]> GetPath(int[] start, int[] end)
    {
        List<int[]> tempList = new List<int[]>();
        int[] temp = { start[0], start[1] };
        tempList.Add(start);
        int stepCanceller = 0;

        while ((temp[0] != end[0] || temp[1] != end[1]))
        {
            // check diaganals



            if ((temp[0] - end[0]) > 0 && (temp[1] - end[1]) <= 0 && OnBoard(temp[0] - 1, temp[1] + ((1 + temp[0]) % 2)))
            {
                if (temp[0] - 1 == end[0] && temp[1] == end[1])
                {
                    temp[0]--;
                }
                else
                {
                    temp[0]--;
                    temp[1] += ((1 + temp[0]) % 2);
                }
                tempList.Add(new int[] { temp[0], temp[1] });
            }
            else if ((temp[0] - end[0]) < 0 && (temp[1] - end[1]) <= 0 && OnBoard(temp[0] + 1, temp[1] + ((2 + temp[0]) % 2)))
            {
                if (temp[0] + 1 == end[0] && temp[1] == end[1])
                {
                    temp[0]++;
                }
                else
                {
                    temp[0]++;
                    temp[1] += ((1 + temp[0]) % 2);
                }
                tempList.Add(new int[] { temp[0], temp[1] });
            }
            else if ((temp[0] - end[0]) > 0 && (temp[1] - end[1]) >= 0 && OnBoard(temp[0] - 1, temp[1] - ((1 + temp[0]) % 2)))
            {
                temp[0]--;
                temp[1] -= (temp[0] % 2);
                tempList.Add(new int[] { temp[0], temp[1] });
            }
            else if ((temp[0] - end[0]) < 0 && (temp[1] - end[1]) >= 0 && OnBoard(temp[0] + 1, temp[1] - ((2 + temp[0]) % 2)))
            {
                temp[0]++;
                temp[1] -= (temp[0] % 2);
                tempList.Add(new int[] { temp[0], temp[1] });
            }
            else if ((temp[1] - end[1]) > 0)
            {
                temp[1]--;
                tempList.Add(new int[] { temp[0], temp[1] });
            }
            else if ((temp[1] - end[1]) < 0)
            {
                temp[1]++;
                tempList.Add(new int[] { temp[0], temp[1] });
            }
            if (stepCanceller == 50)
            {
                return null;
            }
            stepCanceller++;
        }
        return tempList;
    }

    // gets the in world point of a tile
    public Vector3 GetTileMidPoint(int[] tilePosition)
    {
        return fieldTiles[tilePosition[0], tilePosition[1]].GetComponent<SpriteRenderer>().bounds.center;
    }

    // checks if the current field position is empty (null) or not
    public bool CheckObstruction(List<int[]> positions)
    {
        foreach (int[] position in positions)
        {
            if (currentField[position[0], position[1]] != null)
            {
                return false;
            }
        }
        return true;
    }

    // returns true if on board
    bool OnBoard(int x, int y)
    {
        return (x >= 0 && x < length && y >= 0 + ((x + 1) % 2) && y < height);
    }
    public void RemovePiece(Monster piece, bool evil)
    {
        if (evil)
        {
            evilArmy.Remove(piece.gameObject);
        }
        else
        {
            allyArmy.Remove(piece.gameObject);
        }

        RemoveFieldPosition(piece.fieldLocation);
    }

    public void UpdateFieldPosition(int[] startPos, int[] nextPos, GameObject obj)
    {
        if (startPos != null) currentField[startPos[0], startPos[1]] = null;
        if (nextPos != null) currentField[nextPos[0], nextPos[1]] = obj;
        ColourTiles();
    }

    public void RemoveFieldPosition(int[] pos)
    {
        currentField[pos[0], pos[1]] = null;
        ColourTiles();
    }


    // Colour field
    public void ColourTiles()
    {
        foreach (GameObject tile in fieldTiles)
        {
            if (tile != null) tile.GetComponent<SpriteRenderer>().color = new Color(255, 255, 255, 0.2f);
        }

        for (int x = 0; x < length; x++)
        {
            for (int y = 0; y < height; y++)
            {
                if (currentField[x, y] != null)
                {
                    fieldTiles[x, y].GetComponent<SpriteRenderer>().color = new Color(0, 0, 0, 0.6f);
                }
            }
        }
    }

    public void MoveFieldPosition(int[] startPos, int[] nextPos)
    {
        /*
        currentField[nextPos[0], nextPos[1]] = currentField[startPos[0], startPos[1]];
        currentField[startPos[0], startPos[1]] = null;
        */
    }

    // from here down is TODO stuff, but can be ignored for now
    // returns a path from start to end, null if no path
    LinkedList<int[]> AStarSearch(int[] start, int[] end)
    {
        List<int[]> movementList = new List<int[]>();
        PriorityQueue checkableSpaces = new PriorityQueue();
        /*
        foreach (int[] position in GetTilesInRangeOfPosition(start[1], start[0], 1))
        {
          //checkableSpaces.Enqueue(position, Mathf.Abs((position[0] - end[0]) + Mathf.Abs(position[1] - end[0])));
        }
        */

        checkableSpaces.Enqueue(new Pair(start, Mathf.Abs((start[0] - end[0]) + Mathf.Abs(start[1] - end[1]))));
        int[] current;
        while (checkableSpaces.length() > 0)
        {
            // Get the next space to search from
            current = (int[])checkableSpaces.Dequeue().obj;

            List<int[]> adjacents = GetTilesInRangeOfPosition(current[1], current[0], 1);
            foreach (int[] neighbour in adjacents)
            {
                float score = Mathf.Abs((neighbour[0] - end[0]) + Mathf.Abs(neighbour[1] - end[1]));
                if (neighbour[0] == current[0] && neighbour[1] == current[1])
                {
                    score *= 3;
                }
                Pair tempPair = new Pair(neighbour, score);
                tempPair.parent = new Pair(current, 0f);

                if (current[0] == end[0] && current[1] == end[1])
                {
                    return ReconstructPath(tempPair);
                }
                if (currentField[neighbour[0], neighbour[1]] != null)
                    checkableSpaces.Enqueue(tempPair);
            }
        }

        return null;
    }

    // reconstructs path from parents
    LinkedList<int[]> ReconstructPath(Pair goalPair)
    {
        Pair temp = goalPair;
        LinkedList<int[]> path = new LinkedList<int[]>();
        while (temp.parent != null)
        {
            path.AddFirst(temp.obj as int[]);
            temp = temp.parent;
        }

        return path;
    }

    // simple priorityqueue class that serves the A* search
    class PriorityQueue
    {
        LinkedList<Pair> list = new LinkedList<Pair>();
        public void Enqueue(Pair p)
        {

            foreach (Pair pair in list)
            {
                if (pair.weight > p.weight)
                {
                    list.AddBefore(list.Find(pair), p);
                    return;
                }
            }
        }

        public Pair Dequeue()
        {
            Pair tempObject = list.First.Value;
            list.RemoveFirst();
            return tempObject;
        }

        public int length() { return list.Count; }


    }
    class Pair
    {
        public Pair(object o, float w)
        {
            obj = o;
            weight = w;
        }
        public float weight { get; set; }
        public object obj { get; set; }
        public Pair parent { get; set; }
    }
}
