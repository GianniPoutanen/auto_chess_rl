﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Piece : MonoBehaviour
{

    // how large the piece is
    public enum area
    {
        Single,
        Pearce,
        Triangle,
        LargeTriangle,
        Square,
        LargeSquare,
        Huge
    }

    // used to tell other scripts what to do with this piece
    public enum type
    {
        Monster,
        Blockade,
        Trap
    }

    public string pieceName = "";
    public string pieceDescription = "";
    public area size { get; set; }

    // [0] = y, [1] = x
    private int[] fieldLocation { get; set; }

}
