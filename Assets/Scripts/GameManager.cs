﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public enum GameState
    {
        NoBattle,
        TestBattle,
        StartBattle,
        GoodWins,
        EvilWins
    }

    public GameState CurrentState = GameState.NoBattle;

}
