﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExampleBattle : MonoBehaviour
{
    public GameObject monster;

    // Start is called before the first frame update
    void Start()
    {
        // Ally example
        List<GameObject> monsters = new List<GameObject>();
        GameObject monsterone = Instantiate(monster);
        GameObject monstertwo = Instantiate(monster);
        GameObject monsterthree = Instantiate(monster);
        monsterone.GetComponent<Monster>().fieldLocation = new int[] { 0, 1 };
        monstertwo.GetComponent<Monster>().fieldLocation = new int[] { 0, 2 };
        monsterthree.GetComponent<Monster>().fieldLocation = new int[] { 0, 3 };
        monsterone.name += " ally 1";
        monstertwo.name += " ally 2";
        monsterthree.name += " ally 3";
        monsters.Add(monsterone);
        monsters.Add(monstertwo);
        monsters.Add(monsterthree);

        // Enemy example
        List<GameObject> enemies = new List<GameObject>();
        GameObject enemyone = Instantiate(monster);
        GameObject enemytwo = Instantiate(monster);
        GameObject enemythree = Instantiate(monster);
        enemyone.GetComponent<Monster>().fieldLocation = new int[] { 4, 1 };
        enemytwo.GetComponent<Monster>().fieldLocation = new int[] { 4, 2 };
        enemythree.GetComponent<Monster>().fieldLocation = new int[] { 4, 3 };
        enemyone.GetComponent<Monster>().isEvil = true;
        enemytwo.GetComponent<Monster>().isEvil = true;
        enemythree.GetComponent<Monster>().isEvil = true;
        enemyone.name += " evil 1";
        enemytwo.name += " evil 2";
        enemythree.name += " evil 3";
        enemies.Add(enemyone);
        enemies.Add(enemytwo);
        enemies.Add(enemythree);

        this.GetComponent<Field>().InitialiseField(monsters, enemies, 5, 3);
        monsterone.GetComponent<Monster>().field = this.GetComponent<Field>();
        monstertwo.GetComponent<Monster>().field = this.GetComponent<Field>();
        monsterthree.GetComponent<Monster>().field = this.GetComponent<Field>();
        enemyone.GetComponent<Monster>().field = this.GetComponent<Field>();
        enemytwo.GetComponent<Monster>().field = this.GetComponent<Field>();
        enemythree.GetComponent<Monster>().field = this.GetComponent<Field>();
    }
}
