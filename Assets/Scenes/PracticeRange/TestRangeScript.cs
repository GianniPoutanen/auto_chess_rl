﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestRangeScript : MonoBehaviour
{
    public GameObject PracticeTarget;
    public GameObject TestCharacter;
    private GameObject target;
    private Field field;

    void Start()
    {
        field = GameObject.Find("Field").GetComponent<Field>();
        // Ally example
        List<GameObject> monsters = new List<GameObject>();
        GameObject Target = Instantiate(PracticeTarget.gameObject);
        Target.GetComponent<Monster>().fieldLocation = new int[] { GameObject.Find("Field").GetComponent<Field>().length / 2, 1 + GameObject.Find("Field").GetComponent<Field>().height / 2 };
        monsters.Add(Target);

        this.GetComponent<Field>().InitialiseField(null, monsters, 4);
        Target.GetComponent<Monster>().field = this.GetComponent<Field>();
    }


    private void Update()
    {
        if (Input.GetMouseButtonDown(1))
        {
            GameObject target = field.GetObjectUnderMouse();

            if (target != null && target.GetComponent<Monster>() != null)
            {
                field.GetComponent<SpellEffectDictionary>().CastSpellOnTarget(target.GetComponent<Monster>(), SpellEffectDictionary.Spell.LightningBolt, 4);
            }
        }

        if (Input.GetMouseButtonDown(0))
        {
            if (field.GetObjectUnderMouse() == null)
            {
                if (target != null)
                {
                    field.ClearPosition(target.GetComponent<Monster>().fieldLocation);
                    Destroy(target);
                }
                GameObject tile = field.GetTileUnderMouse();
                if (tile != null)
                {
                    for (int x = 0; x < field.length; x++)
                    {
                        for (int y = 0; y < field.height; y++)
                        {
                            if (field.fieldTiles[x, y] == tile)
                            {
                                GameObject.Find("GameManager").GetComponent<GameManager>().CurrentState = GameManager.GameState.NoBattle;
                                field.roundWoundup = Time.time + 1;
                                field.currentField[x, y] = target;
                                field.fieldTiles[x, y].GetComponent<SpriteRenderer>().color = new Color(0, 0, 0, 0.6f);
                                target = Instantiate(TestCharacter.gameObject);
                                target.GetComponent<Monster>().transform.position = field.GetTileMidPoint(new int[] { x, y }) - new Vector3(0, 0, 10);
                                target.GetComponent<Monster>().fieldLocation = new int[] { x, y };
                                // breaks field 
                                x = field.length;
                                y = field.length;
                            }
                        }
                    }
                }
            }
        }
    }
}
