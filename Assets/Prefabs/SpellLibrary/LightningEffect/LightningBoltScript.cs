﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightningBoltScript : MonoBehaviour
{
    public GameObject start;
    public GameObject end;
    public int numRepitions;
    public float chaosFactor;
    private float boltTimer;
    public bool deathTimer;
    public bool alwaysActive;
    public bool trigger;
    public float boltLifeTime;
    public float fadeSpeed;
    private float opacity;

    private System.Random rng = new System.Random();
    private LineRenderer linerenderer;

    // Start is called before the first frame update
    void Start()
    {
        linerenderer = this.GetComponent<LineRenderer>();
        linerenderer.positionCount = 0;
        opacity = 255;
    }

    private void Update()
    {
        Gradient gradient = new Gradient();
        gradient.SetKeys(
            new GradientColorKey[] { new GradientColorKey(new Color(opacity, opacity, opacity), 0.0f),
                                     new GradientColorKey(new Color(opacity, opacity, opacity), 1.0f)},
            new GradientAlphaKey[] { new GradientAlphaKey(255.0f, 0.0f), new GradientAlphaKey(255.0f, 1.0f) }
        );
        opacity = opacity / fadeSpeed;
        if (opacity < 5 && deathTimer)
        {
            Destroy(this.gameObject);
        }


        linerenderer.colorGradient = gradient;

        if (trigger || (alwaysActive && boltTimer < Time.time))
        {
            trigger = false;
            UpdateLineRenderer();
            boltTimer = Time.time + boltLifeTime;
        }
        else if (boltTimer < Time.time)
        {
            if (deathTimer)
            {
                Destroy(this.gameObject);
            }
            linerenderer.positionCount = 0;
        }

    }

    public void UpdateLineRenderer()
    {
        List<Vector3> listPos = SplitBoltPositions(start.transform.position, end.transform.position, numRepitions);
        listPos.Add(end.transform.position);
        Vector3[] newPositions = listPos.ToArray();
        linerenderer.positionCount = newPositions.Length;
        linerenderer.SetPositions(newPositions);
    }

    private List<Vector3> SplitBoltPositions(Vector3 start, Vector3 end, int splitCount)
    {
        if (splitCount > 0)
        {
            Vector3 newPoint = (start + end) * 0.5f;
            float offsetAmount = (end - start).magnitude * chaosFactor;
            newPoint += GetRandomVector(ref start, ref end, offsetAmount);
            List<Vector3> tempList = SplitBoltPositions(start, newPoint, (splitCount - 1));
            tempList.AddRange(SplitBoltPositions(newPoint, end, (splitCount - 1)));
            return tempList;
        }
        else
        {
            List<Vector3> lastVector = new List<Vector3>();
            lastVector.Add(start);
            return lastVector;
        }
    }

    public Vector3 GetRandomVector(ref Vector3 start, ref Vector3 end, float offsetAmount)
    {
        Vector3 directionNormalized = (end - start).normalized;
        // generate random distance
        float distance = (((float)rng.NextDouble() + 0.1f) * offsetAmount);

        // get random rotation angle to rotate around the current direction
        float rotationAngle = ((float)rng.NextDouble() * 360.0f);

        // rotate around the direction and then offset by the perpendicular vector
        return (Quaternion.AngleAxis(rotationAngle, directionNormalized) * GetPerpendicularVector(ref directionNormalized) * distance);
    }
    private Vector3 GetPerpendicularVector(ref Vector3 directionNormalized)
    {
        if (directionNormalized == Vector3.zero)
        {
            return Vector3.right;
        }
        else
        {
            // use cross product to find any perpendicular vector around directionNormalized:
            // 0 = x * px + y * py + z * pz
            // => pz = -(x * px + y * py) / z
            // for computational stability use the component farthest from 0 to divide by
            float x = directionNormalized.x;
            float y = directionNormalized.y;
            float z = directionNormalized.z;
            float px, py, pz;
            float ax = Mathf.Abs(x), ay = Mathf.Abs(y), az = Mathf.Abs(z);
            if (ax >= ay && ay >= az)
            {
                // x is the max, so we can pick (py, pz) arbitrarily at (1, 1):
                py = 1.0f;
                pz = 1.0f;
                px = -(y * py + z * pz) / x;
            }
            else if (ay >= az)
            {
                // y is the max, so we can pick (px, pz) arbitrarily at (1, 1):
                px = 1.0f;
                pz = 1.0f;
                py = -(x * px + z * pz) / y;
            }
            else
            {
                // z is the max, so we can pick (px, py) arbitrarily at (1, 1):
                px = 1.0f;
                py = 1.0f;
                pz = -(x * px + y * py) / z;
            }
            return new Vector3(px, py, pz).normalized;
        }
    }
}
