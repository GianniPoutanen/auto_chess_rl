﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HayEffect : MonoBehaviour
{
    public float delay = 0f;
    private bool falling = true;

    // Use this for initialization
    void Start()
    {
        // TODO set hay falling to be behind if stopping quicker
        float randFallTime = Random.Range(0.0f, 0.4f);
        this.transform.position += new Vector3(0, 0, -0.1f + randFallTime);
        this.GetComponent<Animator>().Play("HayFalling", -1, randFallTime);
    }

    private void Update()
    {
        if (this.falling)
            this.transform.position -= new Vector3(0, 0.001f, 0);
    }

    public void StopMoving()
    {
        this.falling = false;
    }
}
