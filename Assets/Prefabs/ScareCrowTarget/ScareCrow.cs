﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScareCrow : Monster
{
    public GameObject hayObject;
    public float rotationSpeed;

    private void Start()
    {
        // Base stats
        AddStat("CurrentHealth", 999);
        AddStat("Size", 2);

        // Collect variables
        this.animator = this.GetComponent<Animator>();
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        this.field = GameObject.Find("Field").GetComponent<Field>();
        this.isEvil = true;

        // set scale
        this.transform.localScale = new Vector3(isEvil ? -GetStat("Size") : GetStat("Size"), GetStat("Size"), GetStat("Size"));
    }

    private void Update()
    {
        if (this.GetStat("CurrentHealth") < 999)
        {
            this.transform.Rotate(new Vector3(0, 0, (this.GetStat("CurrentHealth") - 999)));
            this.EditStat("CurrentHealth", 999);
            this.HitEffects();
        }
        if (this.transform.rotation.z != 0)
        {
            if (Mathf.Abs( this.transform.rotation.z ) - (Mathf.Abs(this.transform.rotation.z) * rotationSpeed) < 2)
            {
                this.transform.rotation.Set(0,0,0,0);// Vector3(0, 0, 0);
            }
            this.transform.Rotate(new Vector3(0, 0, -this.transform.rotation.z * rotationSpeed));
            //this.transform.localRotation = Quaternion.RotateTowards(this.transform.localRotation, new Quaternion(0, 0, 0, 0), 0.01f);
        }
    }

    private void HitEffects()
    {
        // Spawn a bunch of hay :)
        if (hayObject != null)
        {
            int spawnCount = 5 + Random.Range(1, 5);

            for (int i = 0; i < spawnCount; i++)
            {
                Vector3 randomPos = this.transform.position + new Vector3(0,0.5f,0) + new Vector3(-0.2f + (0.4f * Random.value), -0.4f + (0.6f * Random.value), 0);
                GameObject hay = Instantiate(hayObject);
                hay.transform.position = randomPos;
            }
        }
    }
}