﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExampleItem : Item
{
    public ExampleItem()
    {
        this.effectedStats.Add("Damage", 2);

        this.effectedStats.Add("MaxHealth", -2);
    }
}
